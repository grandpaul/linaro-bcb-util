# linaro-bcb-util

Linaro BCB util is a simple tool to read/write the Android
Bootload Control Block (BCB) under GNU/Linux.

BCB is a partition on the Android system. It is usually called misc.
The partiton controls the boot flow of the Android-capable bootloader.
For example, you can tell the bootloader to boot into recovery or
fastboot on the next reboot.

The util has the following commands:
  * read - read the specified field of BCB
    - Example:
      #### To read the command field
      linaro-bcb-util /dev/disk/by-partlabel/misc read command
  * write - write the specified field of BCB
    - Example:
      #### To write the command field (boot into fastboot mode)
      linaro-bcb-util /dev/disk/by-partlabel/misc write command bootonce-bootloader
  * clear - clear the specified field of BCB
    - Example:
      #### To clear the command field
      linaro-bcb-util /dev/disk/by-partlabel/misc clear command
  * dump - dump the specified field of BCB
      #### To dump the command field
      linaro-bcb-util /dev/disk/by-partlabel/misc dump command
